﻿:Class report_1 : CapXTemplate
    :Include #.CapXUtils

    ∇ Compose
      :Access public
     
      :If _DebugCallbacks←0 ⋄ ∘∘∘ ⋄ :EndIf
     
      Use'ejTreeGrid' ⍝ We will insert it dynamically
     
      Add _.StyleSheet'/Styles/CapX.css'
      Add _.title'CapX - Report 1'
     
      ⍝ Helper functions and objects
      ⍝ hspace←'.hspace'New _.p''
      ⍝ vspace←'.vspace'New _.p''
     
      div←{⍵ New _.div}                                                 ⍝ Make a div
      textspan←{'.textspan'New _.span ⍵}                                ⍝ And a span
      Button←{b⊣(b←New _.Button ⍺).On'click'⍵}                          ⍝ Button with callback
      vert←{⍺←⊣ ⋄ ⍺ New _.StackPanel ⍵}                                 ⍝ Vertical StackPanel
      horz←{⍺←⊣ ⋄ r⊣(r←⍺ New _.StackPanel ⍵).Horizontal←1}              ⍝ Horizontal StackPanel
     
      Add MainMenu
     
      InitData
      sections←MakeScope MakeFilters MakeGroupBy MakeOutput
     
      ⍝ --- Use an Accordion to divide screen into sections
      ac←'panels'Add _SF.ejAccordion
      ac.Titles←'Scope' 'Filter Levels' 'Summarization' 'Output'
      ac.Sections←sections
      ac.Set'enableMultipleOpen' 'true'
    ∇

    ∇ r←MakeScope;assver;end;form;labels;periods;proj;start;tbl
      ⍝ Populate the "Scope" accordion section
     
      form←'frmScope'New _.Form
      periods←,(↓'ZI4,<->'⎕FMT⍪2014+⍳3)∘.,↓'ZI2'⎕FMT⍪⍳12
     
      (start end proj)←{vert(⍵,':')((⍵~' ')New _.Select periods)}¨'Start Period' 'End Period' 'Projection Period'
      proj.Horizontal←1 ⍝ Projection Period horizontal
      assver←horz'Assumptions Version:'('AssVer'New _.Select(⍕¨⍳9))
     
      labels←'Time Period<br>(we are comparing)<br>&nbsp' 'Projection Basis<br>(how projection is made) &nbsp;'
      tbl←form.Add _.Table(labels,⍪(horz start'&nbsp;'end)(vert proj assver))'padding'
     
      r←form
    ∇

    ∇ r←MakeFilters;d;form;lists;save;sp;tbl
      ⍝ Populate the "Filters" accordion section
     
      Filters←0 2⍴0 ⍝ [;1] Filter Level Names, [;2] Values
     
      form←'frmFilters'New _.Form
     
      DomHry←'domstruct'Column'Name'
     
      form.Add MakeListManager'DomHry'DomHry ⍬'Domain/Hierarchy'
     
      form.Add _.h2'Actual Filters'
      form.Add d←div'divActualFilters' ⍝ Created by onSaveFilterLevels
      d.Add textspan'(none defined)'
     
      form.Add _.h2'Filter Summary'
      form.Add d←div'divFilterSummary' ⍝ Created/Updated by onSaveFilterValues
      d.Add textspan'(none defined)'
     
      r←form
    ∇
          
    ∇ r←MakeGroupBy;branches;dom;domains;form;i;max;mid;min;n;names;parents;save;sels;t;tab;values
      r←'frmGroupBy'New _.Form
     
      (names domains parents)←↓[1]'domstruct'Column'Name' 'Domain' 'Parent'
      Domains←∪domains
      ROLLUPS←(domains{⊂⍵}⌸parents)AllBranches¨domains{⊂⍵}⌸names ⍝ Find all possible roll-ups
      values←{3↓∊(⊂' - '),⍪⍵}¨¨ROLLUPS ⍝ For drop-downs
      tab←(n←≢Domains)4⍴⊂''
      tab[;2]←Domains
      names←'GB'∘,¨(⍕¨⍳⍴tab)~¨' '      ⍝ GB<r><c> = GroupBy
      tab[;1 3]←sels←names[;1 3]{⍺('name'⍺)New _.Select(⍵,⍪⍳≢⍵)}¨(⊂⍕¨⍳n),⍪values
      tab[;3].Selected←(≢¨values)↑¨1
      ROLLUPSSEL←↓1,⍪(≢∘⊃¨ROLLUPS)↑¨1  ⍝ Selected branch and levels within it for each domain
      i←(1=≢¨ROLLUPS)/⍳n               ⍝ Domains with only one branch
      tab[i;3]←⊃¨values[i]             ⍝ Replace selects with fixed text if no choices
      sels[;1].Prompt←'-'
      sels.On⊂'change' 'onGroupBySelect'
      tab[;4]←names[;4]{⍺ New _.div(MakeGroupCheckBoxes ⍵)}¨⍳≢ROLLUPS
      tab←'Order' 'Domain' 'Branch' 'Select Summary Levels'⍪tab
      r.Add'FilterInput'New _.Table tab'' 1
     
      save←('btnRunReport')r.Add _.Button'View Report'
      save.On'click' 'onRunReport'
    ∇

    ∇ r←MakeGroupCheckBoxes i;branch;ids;levels;selection
      ⍝ Make a list of checkboxes using i'th elements of ROLLUPS and ROLLUPSSEL
     
      (branch selection)←i⊃ROLLUPSSEL
      levels←(i,branch)⊃ROLLUPS
     
      ids←(⊂'RBCK',(⍕i)),¨⍕¨⍳⍴levels
      r←{(⍵⊃ids)New _.Input'checkbox'(⍵⊃ids)(⍵⊃levels)}¨⍳⍴levels
      (selection/r).Set⊂'checked' 1
    ∇
    
    ∇ r←MakeOutput;form
     
      form←'frmOutput'New _.Form
      'divOutput'form.Add _.div MakeReport
      r←form
    ∇

    ∇ r←MakeReport;data;domain;hdrs;m;order;pif;rollup;sel;table;tv;z
      ⍝ Update the report
     
      :If 2=⎕NC'ROLLUPS'
      :AndIf 0≠≢∊order←Get{'GB',(⍕⍵),'1'}¨⍳6
          m←~order∊''(,'-')
      :AndIf 0≠≢order←(m/⍳⍴order)[⍋m/order] ⍝ order of rollup dimensions
          sel←ROLLUPSSEL[order]
          rollup←⊃,/(2⊃¨sel)/¨(1⊃¨sel)⊃¨ROLLUPS[order]
          domain←(+/¨2⊃¨sel)/Domains[order]
          pif←Table'pif'
     
          :If 0≠≢Filters
              data←pif PIFDomainCols Filters[;1]
              m←⊃∧/(↓[1]data)∊¨Filters[;2]
          :Else ⋄ m←1
          :EndIf
     
          table←(m⌿pif)PIFDomainCols rollup
          data←m⌿'pif'Column'Policies' 'Premium' 'TIV'
     
          data←data,data×[1]1+(¯25+?(≢data)⍴50)÷100 ⍝ random variations up to +/-25%
          (z data)←data TableToTreeItems table
     
          :If 1000<≢z
              r←New _.p(#.Strings.commaFmt≢z),' nodes generated - maximum is 1,000.'
          :Else
              data←(+⌿(z[;1]=1)⌿data)⍪data ⍝ Compute Grand Total
              data←data(÷⍤1)6⍴1000 1000 1000000 ⍝ Scale
              data←data,100×¯1+data[;4 5 6]÷data[;1 2 3] ⍝ compute ratio
              z←0 'Grand Total'⍪z[;1 2] ⋄ z[;1]+←1
              hdrs←,'Act ' 'Proj ' 'Rat '∘.,'Pol' 'Prem' 'TIV'
              hdrs←hdrs,¨(6⍴'(K)' '(K)' '(M)'),3⍴⊂,''
              data←1 #.Strings.commaFmt data
              r←'report1'New _.ejTreeGrid((z[;2],data)('col'∘,¨⍕¨⍳10)(z[;1]))
              r.ColTitles←(⊂'Rollup'),hdrs
              r.CellWidths←180,6 3/90 60
              r.width←900
           ⍝   r.height←450
              'rowHeight'r.Set 20
          :EndIf
     
      :Else
          r←New _.p'Select at least one summary dimension and press <View Report> in the GroupBy tab to generate a report.'
      :EndIf
    ∇
    
    :EndSection ⍝ UI Construction

    :Section Handlers

    ∇ r←onSaveDomHrySelection;keep;new;newlevels;sel
      ⍝ Called when a new set of Filter Levels has been selected
      :Access Public
     
      newlevels←Get'selected'
     
      new←(⍪newlevels),⊂⍬
      Filters←(Filters[;1]∊newlevels)⌿Filters ⍝ Keep old selections if any
      new[new[;1]⍳Filters[;1];2]←Filters[;2]
      Filters←new
     
      sel←'selectOneFilter'New _.Select newlevels
      sel.On'change' 'onSelectOneFilter'
      r←'#divActualFilters'Replace horz(sel'&nbsp;'(div'divFilterValues'))
    ∇

    ∇ r←onSaveFilterValuesSelection;ix
      ⍝ Called when a new set of values has been selected
      :Access Public
     
      ix←Filters[;1]⍳⊂CurrentFilter
      Filters[ix;2]←⊂Get'selected'
      r←UpdateFilterSummary
    ∇

    ∇ r←onSelectOneFilter;all;current;ix;tbl
      ⍝ Called when a new filter is selected
      ⍝ Populates the divFilterValues division
      :Access Public
     
      ix←Filters[;1]⍳⊂CurrentFilter←_value
      current←⊃Filters[ix;2] ⍝ Current selection
      all←(GetDTValues _value)~⊂'' ⍝ Get all possible values for this level∘∘
      tbl←MakeListManager'FilterValues'(all~current)current'Filter Values'
      r←'#divFilterValues'Replace tbl
    ∇

    ∇ r←onGroupBySelect;branches;col;i;row;z
      ⍝ Handle callbacks on all drop-downs in the GroupBy section
      :Access Public
      (row col)←¯1+⎕D⍳¯2↑_what
     
      :If col=3 ⍝ New branch selected
          branches←row⊃ROLLUPS
          i←getNum _value
          (row⊃ROLLUPSSEL)←i((≢i⊃branches)↑1) ⍝ Update branch and level selection (no attempt to remember old values)
          r←('#',(¯1↓_what),'4')Replace(∊(MakeGroupCheckBoxes row).Render)
          ⍝ ↑↑↑ The ∊ ... Render should not be necessary, remove when Brian gives the OK
      :Else ⍝ order changed
          r←''
      :EndIf
    ∇
    ∇ r←onRunReport;checked;t;z
      :Access Public
     
      t←{((4↑⍤1⊢⍵)∧.='RBCK')⌿⍵}'R'_PageData.⎕NL 2
      checked←(⍳¨≢¨2⊃¨ROLLUPSSEL)∊¨¯1+⎕D∘⍳¨t[;5]{⊂⍵}⌸t[;6]
      (2⊃¨ROLLUPSSEL)←checked
     
      r←'#divOutput'Replace MakeReport
      ⍝r,←Execute'$("#report1").data("ejTreeGrid").collapseAll();'
      ⍝r,←⊂(Execute 3('ejAccordion'#._JSS.JQueryOpt'#panels')'selectedItemIndex')
    ∇

    :EndSection ⍝ Handlers

    :Section ⍝ Div Updates

    ∇ r←UpdateFilterSummary;m;tab
      ⍝ Update divFilterSummary with the current selections
     
      tab←(⊂'&nbsp')⍪⍉⍪⍕∘⍪¨Filters[;2]
      tab←({'<b><u>',⍵,'</u></b>'}¨Filters[;1])⍪tab
      tab←(m←(¯1+2×2⊃⍴tab)⍴1 0)\tab
      ((~m)/tab)←⊂'&nbsp;'
      r←'#divFilterSummary'Replace New _.Table tab
    ∇

    :EndSection ⍝ Div Updates

    :Section Application

    ∇ InitData
      :If 0=⎕NC'#.CCADB' ⋄ #.CCADB←GetCCAData ⋄ :EndIf
      DB←#.CCADB
    ∇

    ∇ values←GetDTValues level;table
        ⍝ Find the possible values for a domain level
     
      table←(DB.DT_cols⍳⊂level)⊃DB.DT_tabs   ⍝ Domain Table in question
      values←Sort∪table Column level
    ∇

    ∇ r←findbranches path;children
      ⍝ recursive "subroutine" of AllBranches - refers to globals parents and names
      ⍝
      :If 0=⍴children←(parents∊1↑path)/names
          r←,⊂path
      :Else
          r←⊃,/findbranches¨(⊂¨children),¨⊂path
      :EndIf
    ∇
        
    ∇ r←parents AllBranches names;m;root
      ⍝ Construct all possible branches from list of parents and names
     
      root←,∘⊂¨(m←parents≡¨names)/names
      (parents names)←(~m)∘/¨parents names
      r←⊃,/findbranches¨root
    ∇
        
    :EndSection ⍝ Application

    :Section UI Tools

    ∇ r←MakeListManager(id unsel sel caption);lm;save
      ⍝ Make a list manager called list<id>
      ⍝ With a save button which will call onSave<id>Selection
     
      lm←('list',id)New _.ListManager(unsel sel)
      lm.(Height Gap)←'150px' '10px'
      save←('btn',id,'Save')New _.Button'Save'
      save.On'click'('onSave',id,'Selection')('selected'lm.Right.getItems'')
      save.style←'margin-top: 10px;'
     
      r←New _.StackPanel(lm save)
    ∇

    :EndSection ⍝ UI Tools

:EndClass