﻿:Class assumptions : CapXTemplate
    :Include #.CapXUtils
    :Include #.HTMLUtils

    :Section UI Construction
    ∇ Compose;ac;assfrm;asssel;f;list;sp;spv;t;td;treefrm;typefrm;divAssSel;toprow;botrow
      :Access public
     
      :If _DebugCallbacks←0 ⋄ ∘∘∘ ⋄ :EndIf
     
      Use'ejGrid'
      Use'dimple'
     
      Add _.title'CapX - Edit Assumptions'
      Add _.StyleSheet'/Styles/CapX.css'
     
      ⍝ Helper functions and objects
      hspace←'.hspace'New _.p''
      vspace←'.vspace'New _.p''
     
      div←{⍵ New _.div}                                                 ⍝ Make a div
      textspan←{'.textspan'New _.span ⍵}                                ⍝ And a span
      Button←{b⊣(b←New _.Button ⍺).On'click'⍵}                          ⍝ Button with callback
      vert←{⍺←⊣ ⋄ ⍺ New _.StackPanel(1↓,vspace,⍪⍵)}                     ⍝ Vertical StackPanel
      horz←{⍺←⊣ ⋄ r⊣(r←⍺ New _.StackPanel(1↓,hspace,⍪⍵)).Horizontal←1}  ⍝ Horizontal StackPanel
     
      Add MainMenu ⍝ #.CapXUtils.MainMenu
      InitData
     
      typefrm←'typefrm'Add _.Form
      typefrm.style←'margin: 20px;'
     
      ⍝ --- Assumption Type DropDown ---
     
      divAssSel←div'divAssSel'
      '.boldlabel'divAssSel.Add _.label'Select Assumption Type:'
      asssel←'asstype'divAssSel.Add _.Select DB.AssTypes
      asssel.On'change' 'onSelectAType' ⍝ Rest of form populated when Assumption Type is selected
     
      ⍝ --- Nested StackPanels for layout ---
      toprow←horz(vert divAssSel(div'divFilterInput'))(div'divAEdit')(chartDiv←div'divAChart') ⍝ Assumption type selection / filtering and editing
      botrow←horz(div'divFilterTree')(div'divFilterTable')              ⍝ Fiter Tree and Tables in bottom row
      typefrm.Add vert toprow botrow
      chartDiv.Add ChartCode
    ∇

    ∇ grid←MakeFilterInput cols;btn;l;lab;names;rows;sels;edits
      ⍝ Populates the "High Level" Filter Grid
     
      rows←3 ⍝ Number of grid rows to create
      names←'FG'∘,¨(⍕¨⍳rows 4)~¨' '
      sels←names[;1 2]{⍺('name'⍺)New _.Select ⍵}¨(⍪(≢names)⍴⊂cols),⊂,⊂'[...]'
      sels.On⊂'change' 'onFilterSelect'
      sels,←edits←{⎕NEW _.EditField(⍵'')}¨names[;3]
      edits.On⊂'change' 'onFilterChange'
      sels,←div¨names[;4]
     
      sels←'Column' 'Value' 'Selected Values' 'Policies Found'⍪sels⍪'' '' ''('TotSel'New _.div)
      grid←'FilterInput'New _.Table sels'' 1
     
      MASKS←3⍴⊂⍬
    ∇

    ∇ grid←MakeAEditor(type id);m;tbi
      ⍝ Make assumption edifor for
     
      COLUMNS←⎕NS¨2⍴⊂''
      COLUMNS.field←'Period' 'Factor'
      COLUMNS.headerText←'Period' 'Factor'
      COLUMNS.width←⊂'70px'
      COLUMNS.textAlign←'right' 'right'
      COLUMNS.format←'{0:n0}' '{0:n2}'
      COLUMNS[1].isPrimaryKey←_true
     
      m←((#.Strings.lc¨'assumptions'Column'Type')∊⊂type)∧('assumptions'Column'ID')∊id
      VALUES←AssumptionValues AEDITKEYS←type id
      VALUES←m⌿'assumptions'Column'Period' 'Factor'
     
      grid←'AEditor'New _SF.ejGrid(VALUES COLUMNS)
      (grid.Options.editSettings←⎕NS'').(allowEditing allowAdding allowDeleting)←_true
      'sortSettings.sortedColumns'grid.Set'⍎[{field:"Period"}]'
      grid.Options.editSettings.editMode←'Normal'
     
      tbi←2↓∊', ej.Grid.ToolBarItems.'∘,¨'Add' 'Edit' 'Delete' 'Update' 'Cancel'
      'toolbarSettings'grid.Set'⍎{ showToolbar: true, toolbarItems: [',tbi,']}'
     
    ∇

    ∇ r←ChartCode
      r←_.dimple ScriptFollows
⍝    $(function(){
⍝    var svg = dimple.newSvg("#divAChart", 350, 250);
⍝    myChart = new dimple.chart(svg);
⍝    myChart.data = [{"Period":1,"Factor":50},{"Period":4,"Factor":100},{"Period":9,"Factor":50}];
⍝    myChart.setBounds(0, 0, 350, 250);
⍝    var x = myChart.addCategoryAxis("x", "Period");
⍝    myChart.addMeasureAxis("y", "Factor");
⍝    var s = myChart.addSeries(null, dimple.plot.line);
⍝    s.interpolation = "step";
⍝    myChart.draw();
⍝    });
    ∇

    ∇ r←UpdateChart
    ⍝ Update the chart from global VALUES
      r←Execute'myChart.data = ',#.JSON.fromAPL'Period' 'Factor'#.JSON.fromTable⊃{{⍉↑(⍳⍴⍵)⍵}(1,⍨2-⍨/⍺)/⍵}/↓[1]{⍵[⍋⍵;]}VALUES
      r,←Execute'myChart.draw()'
      r,←Execute'#divAChart'Css'display' 'block'
    ∇
    :EndSection ⍝ UI Construction

    :Section Handlers

    ∇ r←onSelectAType;asstype;cols;ua;d;s
      ⍝ Called when Assumption Type is changed
     
      :Access Public
      ASSTYPE←#.Strings.lc _value
     
      cols←Sort DB.DT_cols
      r←'#divFilterInput'Replace MakeFilterInput cols
     
      ua←{⍵[⍋⍵]}∪(('assumptions'Column'Type')∊⊂_value)⌿'assumptions'Column'ID'
     
      d←div'divAssSel'
      '.boldlabel'd.Add _.label('Select ',_value,' Assumption:')
      s←'asssel'd.Add _.Select(⍕¨ua)
      s.On'change' 'onSelectAssumption' ⍝ divAEGrid populated when Assumption is selected
      r,←'#divAEdit'Replace(vert d(div'divAEGrid')).Render
    ∇

    ∇ r←onSelectAssumption;content;grid;id;msgdiv;savebtn
        ⍝ Called when an assumption is selected
      :Access Public
     
      Assumption←getNum _value
      grid←MakeAEditor ASSTYPE Assumption
      savebtn←'SaveData'New _.Button'Save'
      savebtn.style←'margin-top: 10px;'
      savebtn.On'click' 'onAEditSave'('ViewData'grid.getModel'currentViewData')
      (msgdiv←'divAssMsg'New _.div).style←'width: 150px;'
      r←'#divAEGrid'Replace _.StackPanel(grid savebtn msgdiv)
     
      r,←UpdateFilterAction
      r,←UpdateChart
    ∇

    ∇ r←onNodeSelect;item;node
        ⍝ Called when a tree node is selected
      :Access Public
     
      :If _what≡'reset' ⋄ node←0 ⍝ Click on caption
      :Else ⋄ node←TreeItems[;3]⍳⊂Get'node' ⋄ :EndIf
      r←UpdateFilterTable node
    ∇

    ∇ r←onFilterChange
        ⍝ Called when a filter edit is modified
      :Access Public
     
      r←FilterPolicies _what _value
    ∇

    ∇ r←onFilterSelect;type;cols;i;values;sel;id;edit;val;table;fgr;rowid;col
        ⍝ Called when one of the drop-downs in the Filter area is used
     
      :Access Public
     
      :If 'FG'≡2↑_what ⍝ drop-downs are named FGrc where rc = row,col number
          col←getNum 4⊃_what
     
          :Select col
     
          :Case 1 ⍝ New filter column selected
              rowid←'#',3↑_what ⍝ update dropdown in 2nd column
              table←(DB.DT_cols⍳⊂_value)⊃DB.DT_tabs   ⍝ "DT" table in question
              values←(⊂'[Select Value]'),Sort∪table Column _value
     
              r←(rowid,'2')Replace _.Select.FormatOptions values ⍝ Update items in the dropdown
              r,←Execute(rowid,'3')_JSS.Val''   ⍝ Clear edit field
              r,←(rowid,'4')Replace''           ⍝ Clear row counter
     
          :Case 2 ⍝ New filter value => add to values
              fgr←3↑_what
              val←Get id←fgr,'3'                ⍝ Current contents of edit field
              val←#.Strings.dmb _value,' ',val  ⍝ Append new value
              r←Execute('#',id)_JSS.Val val     ⍝ Update edit field
              r,←FilterPolicies _what val       ⍝ Update PIF grid & tree
          :EndSelect
      :EndIf
    ∇

    ∇ r←onAEdit;add;f;i;p
      ⍝ Called when row added, changed or deleted in Assumption editor
      ⍝ ↓↓↓ This function is no longer used, it was an attempt to trace
      ⍝ ↓↓↓ Editor state on the server side, which turned out to be unnecessary
      ⍝ ↓↓↓ We just call onAEditSave with client-side model data
      :Access Public
     
      r←''
      (p f)←getNum¨_PageData.editcell.(Period Factor)
     
      :Select _event
      :Case 'endAdd'
          VALUES⍪←p f
      :Case 'endEdit'
          :If (≢VALUES)≥i←VALUES[;1]⍳p ⋄ VALUES[i;2]←f
          :Else ⋄ ∘∘∘ ⍝ Paranoia: Update but key not found
          :EndIf
      :Case 'endDelete'
          VALUES←((⍳≢VALUES)≠VALUES⍳p f)⌿VALUES
      :Else
          ∘∘∘
     
      :EndSelect
      ⎕←VALUES⍪'-'
    ∇

    ∇ r←onAEditSave
      :Access Public
     
      VALUES←↑(Get'ViewData').(Period Factor)
      VALUES AssumptionValues AEDITKEYS ⍝ Save the data
      r←'#divAssMsg'Replace _.p('Assumption ',(⍕2⊃AEDITKEYS),' updated.')
      r,←UpdateChart
    ∇
        
    ∇ r←onModifyFilter
        ⍝ Called when the used presses a button below the filter table
      :Access Public
      ⎕←'Button clicked - left as an exercise for Chris :-)'
     
    ∇

    :EndSection ⍝ Handlers

    :Section ⍝ Div Updates
    ∇ r←UpdateTree;cols;filters;filters_pif;hdr;i;items;m;new;pif;pif_cols;tbl;tgt_col;tgt_cols;tp;tree_data;tv
      ⍝ Update the grid and tree view representing the current filter
     
      r←''
     
      :If 2=⎕NC'MASKS'
      :AndIf ∨/m←⊃∧/(0≠≢¨MASKS)/MASKS ⍝ Current PIF selection
          pif←m⌿Table'pif'
          filters←Table tbl←'filters_',#.Strings.lc ASSTYPE
          cols←Columns tbl                           ⍝ Domain columns in the filter table
          filters_pif←pif PIFDomainCols cols         ⍝ Values corresponding to each, from the PIF table
     
          tgt_cols←(~cols∊'ID' 'Assumption')/⍳≢cols
          m←⊃∧/(↓[1]filters[;tgt_cols])∊¨↓[1]filters_pif[;tgt_cols]⍪⊂''
          FilterTable←cols⍪m⌿filters
     
          ⍝ Create the tree
          TreeItems←TableToTreeItems filters_pif[;tgt_cols]
          TreeItems←(1 1↓0 ¯1↓FilterTable)AddCounts TreeItems
          tv←'FilterTree'New #._.ejTreeView(0 ¯1↓TreeItems)
          tv.On'nodeSelect' 1
          tv.On'nodeSelect' 'onNodeSelect'('node' 'eval' 'argument.id')
          hdr←'reset .boldlabel'New _.label('Filter Tree (',(⍕¯1+≢FilterTable),')')
          hdr.On'click' 'onNodeSelect'
          r←'#divFilterTree'Replace(New _DC.StackPanel(hdr'<br>'tv)).Render
     
          r,←UpdateFilterTable 0 ⍝ Select Node 0 (all)
      :EndIf
    ∇

    ∇ r←UpdateFilterTable node;hdr;m;path;table;tbl
      :If node=0 ⋄ table←FilterTable
      :Else
          path←⌽(node+1)-(⌽node↑TreeItems[;1])⍳⌽⍳TreeItems[node;1]
          TreePath←TreeItems[path;5]
          m←FilterTable[;1+⍳≢path]∧.≡TreePath
          table←(1,1↓m)⌿FilterTable
      :EndIf
     
      tbl←'FilterTable'New _.Table table'' 1
      hdr←'.boldlabel'New _.label(⍕¯1+≢table),' of ',(⍕¯1+≢FilterTable),' Filters Selected'
      r←'#divFilterTable'Replace(vert hdr tbl(div'divFilterAction')).Render
      r,←UpdateFilterAction
    ∇

    ∇ r←UpdateFilterAction;text;path
        ⍝ Update the "Filter Action" div with currently possible actions
     
      :If 2=⎕NC'FilterTable'
          :If Assumption≡''
              r←textspan'To add a filter, select an assumption.'
          :Else
              text←'Use assumption ',⍕Assumption
              :If ' '∨.≠path←⍕∊1↓,(⊂' / '),⍪TreePath
                  path←' for ',path
              :Else
                  path←' as the default.'
              :EndIf
              r←(text Button'onModifyFilter')(textspan path)
          :EndIf
     
          r←'#divFilterAction'Replace∊r.Render
      :Else
          r←'#divFilterAction'Replace textspan''
      :EndIf
    ∇

    :EndSection ⍝ Div Updates

    :Section Application

    ∇ InitData
      :If 0=⎕NC'#.CCADB' ⋄ #.CCADB←GetCCAData ⋄ :EndIf
      DB←#.CCADB
      ASSTYPE←''
      MASKS←⍬ ⍬ ⍬
      TreePath←''
      Assumption←''
    ∇

    ∇ r←FilterPolicies(_what _value);fgr;colname;val;pifcol;m;row
        ⍝ Filter Policies in Force
     
      fgr←3↑_what
      row←getNum 3⊃_what
      colname←Get fgr,'1'
      (val pifcol)←FilterPIFs({1↓¨(⍵=' ')⊂⍵}' ',_value)colname
      MASKS[row]←⊂m←(,'pif'Column pifcol)∊val ⍝ Update mask corresponding to this row
     
      r←('#',fgr,'4')Replace(⍕+/m),' &nbsp' ⍝ update the PIF counter in column 4
      m←⊃∧/(0≠≢¨MASKS)/MASKS            ⍝ AND all the filters together
      r,←('#TotSel')Replace{(⍕+/⍵),' of ',⍕≢⍵}m ⍝ Update total count
     
      r,←UpdateTree
    ∇

    ∇ (val pifcol)←FilterPIFs(val col);data;dt_tab
      ⍝ Map values in a DT column to corresponding column and values in PIF table
     
      dt_tab←(DB.DT_cols⍳⊂col)⊃DB.DT_tabs
      pifcol←{(⍵∊Columns dt_tab)/⍵}Columns'pif'
      data←dt_tab Column pifcol,⊂colname
      val←(data[;2]∊val)/data[;1] ⍝ Translate to pif column values
    ∇

    ∇ tree←filters AddCounts tree;col;count;depth;dsel;id;m;nodes;tf;z;i
        ⍝ Given a tree representing Policies in Force
        ⍝ And an existing filter table
        ⍝ Add counts of filter rows pertaining to each tree node
     
      depth←2⊃⍴filters
      tf←((≢tree),depth)⍴⊂'' ⍝ Tree filters
      tree←tree,tree[;2]     ⍝ Keep 4th column with original values
     
      :For col :In ⍳2⊃⍴filters ⍝ For each tree depth
          m←1,1↓dsel←tree[;1]=col
          dsel←dsel/⍳≢dsel ⍝ nodes at this depth
          tf[;col]←(≢¨m⊂m)/m/tree[;2]
          z←filters[;⍳col]
          :If 0≠≢z←(~∨/z∊⊂'')⌿z
              (id count)←↓⍉{⍺(≢⍵)}⌸z
              id←↑id
              i←tf[dsel;⍳col]⍳id
              m←i≤≢dsel
              tree[dsel[m/i];2]←tree[dsel[m/i];2],¨{' (',(⍕⍵),')'}¨m/count
          :EndIf
      :EndFor
    ∇

    :EndSection ⍝ Application

:EndClass