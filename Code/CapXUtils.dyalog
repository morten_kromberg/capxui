﻿:Namespace CapXUtils

    assert←{⎕SIGNAL ⍵↓11}

    ∇ r←{data}TableToTreeItems table;d;depth;depths;g;i;items;k;keys;lowest;m;max;nodes;p;sum;t;totals
    ⍝ Creates an argument suitable for making a tree from a table
     
      table←table[g←⍋⍕table;]
      :If 2=⎕NC'data' ⋄ data←data[g;] ⋄ :EndIf
     
      nodes←,∨\1⍪2≢⌿table                ⍝ We need nodes at unique values
     
      items←(+/nodes)4⍴⊂''
      keys←nodes⌿(2⊃⍴table)⌿table
      items[;1]←depths←nodes/(⍴nodes)⍴⍳2⊃⍴table ⍝ Depths
      items[;2]←nodes/,table             ⍝ Text
      items[;3]←'i',∘⍕¨⍳≢items           ⍝ Label them
     
      :If 2=⎕NC'data' ⍝ we need to summarize the data
          totals←((≢items),2⊃⍴data)⍴0
          m←depths=max←2⊃⍴table          ⍝ location of lowest level of totals
          (m⌿totals)←table{+⌿⍵}⌸data     ⍝ Compute lowest level of totals
     
          :For depth :In ⌽⍳max-1         ⍝ Compute subtotals, bottom up
              m←depths∊depth+0 1
              p←m⌿d←depths=depth
              totals[d/⍳⍴d;]←↑+⌿¨p⊂[1]m⌿totals
          :EndFor
          r←items totals
      :Else ⋄ r←items
      :EndIf
    ∇
    
    ∇ filters_pif←pif PIFDomainCols cols;dt_cols;dt_data;dt_tab;pif_col;pif_cols;pif_vals;tgt_col;tgt_cols
    ⍝ Extract selected columns of PIF, translated to nearest Domain Table Column
     
      filters_pif←((≢pif),≢cols)⍴⊂''
      pif_cols←DB.pif_cols
     
      :For dt_tab :In ∪(DB.DT_cols∊cols)/DB.DT_tabs ⍝ DT tables involved
          dt_cols←Columns dt_tab
          :If 1≠≢pif_col←dt_cols∩pif_cols ⋄ ∘∘∘ ⋄ :EndIf
          pif_vals←pif[;pif_cols⍳pif_col]
          dt_data←Table dt_tab
          :For tgt_col :In tgt_cols←(cols∊(Columns dt_tab)~'ID' 'Assumption')/⍳≢cols
              filters_pif[;tgt_col]←dt_data[(,dt_data[;dt_cols⍳pif_col])⍳,pif_vals;dt_cols⍳cols[tgt_col]]
          :EndFor
      :EndFor
    ∇
    
    ∇ r←{new}AssumptionValues(type id);Type;m
      ⍝ get/Set assumption values for
     
      m←((#.Strings.lc¨'assumptions'Column'Type')∊⊂type)∧('assumptions'Column'ID')∊id
      m←m∧('assumptions'Column'Set' 'Ver')∧.=1
      :If 0=⎕NC'new' ⍝ Get
          r←m⌿'assumptions'Column'Period' 'Factor'
      :Else ⍝ Set
          Type←{((#.Strings.lc¨⍵)⍳⊂type)⊃⍵}DB.AssTypes ⍝ de-lc Type
          assert DB.assumptions_cols≡'Type' 'Set' 'ID' 'Ver' 'Period' 'Factor'
          DB.assumptions←(Type 1 id 1,⍤1⊢new)⍪(~m)⌿DB.assumptions
      :EndIf
    ∇
    
    ∇ r←MainMenu;depth;links;text
     ⍝ Use Add MainMenu at top of each page
     
      text←'Reports' 'Report 1' 'Configuration' 'Assumptions' 'About'
      depth←1 2 1 2 1
      links←'' '/report_1' '' '/assumptions' '/about'
      r←_.ejMenu(text depth links) ⍝ Banner will be pushed before this
    ∇
    
    ∇ r←getNum x
      r←⊃2⊃⎕VFI x
    ∇
    
    ∇ {x}←ok x
    ⍝ Check SQAPL Return code
      →(0≥⊃x)⍴0
      (⍕x)⎕SIGNAL 11
    ∇

    ∇ r←Sort r
    ⍝ Sort lists of char vecs
      r←r[⍋↑r]
    ∇

    ∇ DB←GetCCAData;tbl;data;cols;tn;t;tabs
    ⍝ Materialise ALL tables in DB namespace
     
      ⎕←'NB: using cached SQL data'
      tn←(#.Boot.AppRoot,'ccadb')⎕FSTIE 0
⍝      tn←'d:\devt\capx\ccadb'⎕FSTIE 0
      DB←⎕FREAD tn,1
      ⎕FUNTIE tn
      →0
     
      ok #.SQA.Init''
      {}#.SQA.Close'CCA' ⋄ ok #.SQA.Connect'CCA' 'CCA'
      DB←⎕NS''
      DB.Tables←1↓(2⊃#.SQA.Tables'CCA')[;3]
      :For tbl :In DB.Tables~⊂'elts'
          data←3 1⊃ok #.SQA.Do'CCA'('select * from ',tbl)
          ⍎'DB.',tbl,'←data'
          cols←{1↓⍵[;⍵[1;]⍳⊂'COLUMN_NAME']}2⊃ok #.SQA.Columns'CCA'tbl
          ⍎'DB.',tbl,'_cols←cols'
      :EndFor
     
      DB.AssTypes←∪'assumptions'Column'Type'
     
      DB.DT_cols←⊃,/cols←(Columns¨tabs←{((3↑¨⍵)∊⊂'dt_')/⍵}DB.Tables)~¨⊂⊂'ID'
      DB.DT_tabs←(≢¨cols)/tabs
    ∇
    
    ∇ r←tbl ColIndex col
      :If 80=⎕DR col ⋄ col←,⊂col ⋄ :EndIf
      r←(DB⍎tbl,'_cols')⍳col
    ∇
   
    ∇ r←tbl Column col;i
      :If 80=⎕DR col ⋄ col←⊂col ⋄ :EndIf
      i←(DB⍎tbl,'_cols')⍳col
      r←(DB⍎tbl)[;i]
    ∇

    ∇ r←Columns tbl
      r←DB⍎tbl,'_cols'
    ∇
    
    ∇ r←Table tbl
      r←DB⍎tbl
    ∇

:EndNamespace